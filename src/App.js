import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Homepage from "./Components/Homepage";
import Movies from "./Components/Movies";

function App() {
  const [moviesList, setMoviesList] = React.useState([]);

  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Homepage setMoviesList={setMoviesList} />
        </Route>

        <Route path="/movies">
          <Movies moviesList={moviesList} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
