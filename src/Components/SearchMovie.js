import React from "react";
import { useHistory } from "react-router-dom";

const SearchMovie = ({ setMoviesList }) => {
  let history = useHistory();

  const [movieName, setMovieName] = React.useState("");

  const handleClick = () => {
    fetch(
      `https://api.themoviedb.org/3/search/movie?api_key=dd8287c48444ec5989a8a343a5eb1674&query=${movieName}`
    ).then((response) => {
      response.json().then((data) => {
        console.log(data.results);
        setMoviesList(
          data.results.map((value) => {
            return {
              id: value.id,
              title: value.title,
              poster: value.poster_path,
              overview: value.overview,
              releaseDate: value.release_date,
            };
          })
        );
      });
    });
    history.push("movies");
  };

  const handleChange = (event) => {
    setMovieName(event.target.value);
  };

  return (
    <div className="search">
      <input
        type="text"
        className="form_input"
        placeholder="Movie name"
        required
        value={movieName}
        onChange={handleChange}
      />
      <button onClick={handleClick} className="search-button">
        <i className="arrow right"></i>
      </button>
    </div>
  );
};

export default SearchMovie;
