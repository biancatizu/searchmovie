import React from "react";

// image={
//   movie.poster && `https://image.tmdb.org/t/p/w500${movie.poster}`
// }

const Movies = ({ moviesList }) => {
  return (
    <div className="cards-list">
      {moviesList.map((movie) => {
        return (
          <div className="card" key={movie.id}>
            <img
              className="card-image"
              src={
                movie.poster
                  ? `https://image.tmdb.org/t/p/w500${movie.poster}`
                  : `https://img.freepik.com/free-vector/404-error-page-found_41910-364.jpg?size=626&ext=jpg`
              }
              alt={`${movie.title} poster`}
            />
            <div className="card-content">
              <p className="card-title">{movie.title}</p>
              <p>
                <small>RELEASE DATE: {movie.releaseDate}</small>
              </p>
            </div>

            <div className="card-over">
              <p>{movie.overview}</p>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default Movies;
