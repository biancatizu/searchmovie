import React from "react";
import SearchMovie from "./SearchMovie";

const Homepage = ({setMoviesList}) => {
  return (
    <div className="main-container">
      <div className="title-container">
        <h1>Movies Search</h1>
        <h1>Movies Search</h1>
      </div>
      <SearchMovie setMoviesList={setMoviesList}/>
    </div>
  );
};

export default Homepage;
