<h1 align="center">Movies search</h1>

<p>A simple movie search app with React, that uses api.themoviedb.org to gather information</p>

<h3 align="center">How to use it?</h3>
<p>In the homepage, insert a movie keyword and submit it.</p>

![](src/assets/homepage.PNG)

<p>Then you will see a page with all the movies that contain your keyword in title. When you hover over a poster, you can check the overview of the selected movie.</p>

![](src/assets/search-page.png)

<h3 align="center">Project setup</h3>
<ul>
    <li>npm install</li>
    <li>npm start</li>
</ul>

<h3 align="center">Technologies</h3>
<p>Project is created with <b>React</b> and integrates Themoviedb API with Fetch API.</p>

<h3 align="center">Inspiration</h3>
<p>Project was inspired by a Scrimba Tutorial.</p>
